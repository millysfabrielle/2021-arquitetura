package com.example.arquitetura.config.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExecptionHandlerController {

    @ExceptionHandler(value = {CustomValidationException.class})
    public ResponseEntity<?> customValidationException( CustomValidationException e){

        ValidationExceptionResponse r = new ValidationExceptionResponse( e.getField(), e.getMessage());

        return new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
    }

}
