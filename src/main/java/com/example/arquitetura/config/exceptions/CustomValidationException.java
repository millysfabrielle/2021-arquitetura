package com.example.arquitetura.config.exceptions;

public class CustomValidationException extends RuntimeException{

    private String field;
    private String message;

    public CustomValidationException() { }

    public CustomValidationException(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
