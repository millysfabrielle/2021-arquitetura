package com.example.arquitetura.repository;

import com.example.arquitetura.entity.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    public Pessoa findByCpf(String cpf);

    @Query("select p from Pessoa p where p.cpf = ?1")
    public Pessoa findByCpfQuery(String cpf);

}
