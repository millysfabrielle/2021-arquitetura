package com.example.arquitetura.repository;

import com.example.arquitetura.entity.PedidoTemProduto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PedidoTemProdutoRepository extends JpaRepository<PedidoTemProduto, Long> {
}
