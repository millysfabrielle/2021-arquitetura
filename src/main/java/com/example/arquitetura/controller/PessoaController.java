package com.example.arquitetura.controller;

import com.example.arquitetura.entity.Pessoa;
import com.example.arquitetura.service.PessoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    PessoaService pessoaService;

    @GetMapping
    public ResponseEntity<?> buscar(){
        List<Pessoa> pessoas = pessoaService.buscar();
        return new ResponseEntity<>(pessoas, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> buscarPeloId(@PathVariable Long id){
        Pessoa pessoa = pessoaService.buscaPeloId(id);
        return new ResponseEntity<>(pessoa, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> salvar(@RequestBody Pessoa pessoa){

        Pessoa pessoaComId = pessoaService.salvar(pessoa);
        return new ResponseEntity<>(pessoaComId, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> atualizar(@RequestBody Pessoa pessoa){
        Pessoa pessoaAtualizada = pessoaService.salvar(pessoa);
        return new ResponseEntity<>(pessoaAtualizada, HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> excluir(@PathVariable Long id){
        boolean resposta = pessoaService.excluir(id);
        return new ResponseEntity<>(resposta, HttpStatus.OK);
    }

}
