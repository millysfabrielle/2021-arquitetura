package com.example.arquitetura.entity;

import javax.persistence.*;

@Entity
@Table(name="endereco")
public class Endereco {

    @Id
    @GeneratedValue( strategy=GenerationType.AUTO )
    @Column(name="id")
    private long id;

    @Column(name="logradouro")
    private String logradouro;

    @Column(name="complemento")
    private String complemento;

    @Column(name="cep")
    private String cep;

    @ManyToOne
    @JoinColumn(name = "pessoa_id")
    private Pessoa pessoa;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }
}
