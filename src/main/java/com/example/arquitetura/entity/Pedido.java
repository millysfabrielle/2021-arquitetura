package com.example.arquitetura.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "pedido")
public class Pedido {

    @Id
    @GeneratedValue( strategy= GenerationType.AUTO )
    @Column(name = "id")
    private long id;

    @Column(name = "numero")
    private String numero;

    @Column(name = "data")
    private Date data;

    @ManyToOne
    @JoinColumn(name = "pessoa_id")
    private Pessoa pessoa;

    @OneToMany(mappedBy = "pedido")
    List<PedidoTemProduto> pedidoTemProdutos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public List<PedidoTemProduto> getPeditoTemProdutos() {
        return pedidoTemProdutos;
    }

    public void setPeditoTemProdutos(List<PedidoTemProduto> pedidoTemProdutos) {
        this.pedidoTemProdutos = pedidoTemProdutos;
    }
}
