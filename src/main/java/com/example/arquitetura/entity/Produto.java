package com.example.arquitetura.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "produto")
public class Produto {

    @Id
    @GeneratedValue( strategy= GenerationType.AUTO )
    @Column(name="id")
    private long id;

    @Column(name="nome")
    private String nome;

    @Column(name="descricao")
    private String descricao;

    @Lob
    @Column(name = "img")
    private String img;

    @Column(name="preco")
    private float preco;

    @ManyToMany
    @JoinTable(
            name = "categoria_tem_produto",
            joinColumns = @JoinColumn(name = "produto_id"),
            inverseJoinColumns = @JoinColumn(name = "categoria_id")
    )
    private List<Categoria> categorias;


    @OneToMany(mappedBy = "produto")
    private List<PedidoTemProduto> pedidoTemProdutos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    public List<PedidoTemProduto> getPeditoTemProdutos() {
        return pedidoTemProdutos;
    }

    public void setPeditoTemProdutos(List<PedidoTemProduto> pedidoTemProdutos) {
        this.pedidoTemProdutos = pedidoTemProdutos;
    }
}
