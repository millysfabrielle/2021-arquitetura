package com.example.arquitetura.entity;

import javax.persistence.*;

@Entity
@Table(name = "pedito_tem_produto")
public class PedidoTemProduto {

    @Id
    @GeneratedValue( strategy= GenerationType.AUTO )
    @Column(name="id")
    private long id;

    @Column(name="preco")
    private float preco;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "pedido_id")
    private Pedido pedido;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name="produto_id")
    private Produto produto;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
}
