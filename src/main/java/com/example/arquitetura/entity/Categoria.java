package com.example.arquitetura.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="categoria")
public class Categoria {

    @Id
    @GeneratedValue( strategy= GenerationType.AUTO )
    @Column(name="id")
    private long id;

    @Column(name = "nome")
    private String nome;

    @ManyToMany(mappedBy = "categorias")
    private List<Produto> produtos;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}
