package com.example.arquitetura.service;

import com.example.arquitetura.entity.Produto;
import com.example.arquitetura.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProdutoService {

    // Injeção de Dependência
    @Autowired
    ProdutoRepository ProdutoRepository;


    public List<Produto> buscar(){
        return ProdutoRepository.findAll();
    }

    public Produto buscaPeloId(Long id){
        return ProdutoRepository.getOne(id);
    }

    public Produto salvar(Produto Produto){
        return ProdutoRepository.save(Produto);
    }

    public Produto atualizar(Produto Produto){
        return ProdutoRepository.save(Produto);
    }

    public boolean excluir(Long id){
        ProdutoRepository.deleteById(id);
        return true;
    }


}