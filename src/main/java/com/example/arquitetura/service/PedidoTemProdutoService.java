package com.example.arquitetura.service;

import com.example.arquitetura.entity.PedidoTemProduto;
import com.example.arquitetura.repository.PedidoTemProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PedidoTemProdutoService {

    // Injeção de Dependência
    @Autowired
    PedidoTemProdutoRepository PedidoTemProdutoRepository;


    public List<PedidoTemProduto> buscar(){
        return PedidoTemProdutoRepository.findAll();
    }

    public PedidoTemProduto buscaPeloId(Long id){
        return PedidoTemProdutoRepository.getOne(id);
    }

    public PedidoTemProduto salvar(PedidoTemProduto PedidoTemProduto){
        return PedidoTemProdutoRepository.save(PedidoTemProduto);
    }

    public PedidoTemProduto atualizar(PedidoTemProduto PedidoTemProduto){
        return PedidoTemProdutoRepository.save(PedidoTemProduto);
    }

    public boolean excluir(Long id){
        PedidoTemProdutoRepository.deleteById(id);
        return true;
    }


}
