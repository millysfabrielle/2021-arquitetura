package com.example.arquitetura.service;

import com.example.arquitetura.entity.Endereco;
import com.example.arquitetura.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoService {

    // Injeção de Dependência
    @Autowired
    EnderecoRepository EnderecoRepository;


    public List<Endereco> buscar(){
        return EnderecoRepository.findAll();
    }

    public Endereco buscaPeloId(Long id){
        return EnderecoRepository.getOne(id);
    }

    public Endereco salvar(Endereco Endereco){
        return EnderecoRepository.save(Endereco);
    }

    public Endereco atualizar(Endereco Endereco){
        return EnderecoRepository.save(Endereco);
    }

    public boolean excluir(Long id){
        EnderecoRepository.deleteById(id);
        return true;
    }


}