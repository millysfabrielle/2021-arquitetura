package com.example.arquitetura.service;

import com.example.arquitetura.entity.Pedido;
import com.example.arquitetura.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PedidoService {

    // Injeção de Dependência
    @Autowired
    PedidoRepository PedidoRepository;


    public List<Pedido> buscar(){
        return PedidoRepository.findAll();
    }

    public Pedido buscaPeloId(Long id){
        return PedidoRepository.getOne(id);
    }

    public Pedido salvar(Pedido Pedido){
        return PedidoRepository.save(Pedido);
    }

    public Pedido atualizar(Pedido Pedido){
        return PedidoRepository.save(Pedido);
    }

    public boolean excluir(Long id){
        PedidoRepository.deleteById(id);
        return true;
    }


}