package com.example.arquitetura.service;

import com.example.arquitetura.entity.Categoria;
import com.example.arquitetura.repository.CategoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaService {

    // Injeção de Dependência
    @Autowired
    CategoriaRepository CategoriaRepository;


    public List<Categoria> buscar(){
        return CategoriaRepository.findAll();
    }

    public Categoria buscaPeloId(Long id){
        return CategoriaRepository.getOne(id);
    }

    public Categoria salvar(Categoria Categoria){
        return CategoriaRepository.save(Categoria);
    }

    public Categoria atualizar(Categoria Categoria){
        return CategoriaRepository.save(Categoria);
    }

    public boolean excluir(Long id){
        CategoriaRepository.deleteById(id);
        return true;
    }


}