package com.example.arquitetura.service;

import com.example.arquitetura.config.exceptions.CustomValidationException;
import com.example.arquitetura.entity.Pessoa;
import com.example.arquitetura.repository.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PessoaService {

    // Injeção de Dependência
    @Autowired
    PessoaRepository pessoaRepository;


    public List<Pessoa> buscar(){
        return pessoaRepository.findAll();
    }

    public Pessoa buscaPeloId(Long id){
        return pessoaRepository.getOne(id);
    }

    public Pessoa salvar(Pessoa pessoa){

        Pessoa buscaPessoa = pessoaRepository.findByCpfQuery(pessoa.getCpf());

        if(buscaPessoa == null){
            return pessoaRepository.save(pessoa);
        }
        else {
            throw new CustomValidationException("cpf", "CPF duplicado!");
        }


    }

    public boolean excluir(Long id){
        pessoaRepository.deleteById(id);
        return true;
    }

}
